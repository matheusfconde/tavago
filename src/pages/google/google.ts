import { VagaService } from "./../../providers/vaga/vaga.service";
import { Component } from "@angular/core/";

import {
  GoogleMaps,
  GoogleMap,
  GoogleMapsEvent,
  GoogleMapOptions,
  CameraPosition,
  MarkerOptions,
  Marker
} from "@ionic-native/google-maps";
import { NavController } from "ionic-angular";
import { FirebaseListObservable } from "angularfire2";
import { Vaga } from "./../../models/vaga.model";

@Component({
  selector: "page-google",
  templateUrl: "google.html"
})
export class GooglePage {
  map: GoogleMap;
  mapElement: HTMLElement;
  constructor(
    public navCtrl: NavController,
    private googleMaps: GoogleMaps,
    public VagaService: VagaService
  ) {}

  vagas: FirebaseListObservable<Vaga[]>;

  ionViewDidLoad() {
    this.loadMap();
    console.log("mo bosta");
    //pode deltar
    this.vagas = this.VagaService.vagas;
  }

  onListVaga(vaga: Vaga): void {
    console.log("Vaga: ", vaga);
  }

  loadMap() {
    this.mapElement = document.getElementById("map");

    let mapOptions: GoogleMapOptions = {
      camera: {
        target: {
          lat: -19.92494,
          lng: -44.08436
        },
        zoom: 18,
        tilt: 30
      }
    };

    this.map = this.googleMaps.create(this.mapElement, mapOptions);

    // Wait the MAP_READY before using any methods.
    this.map.one(GoogleMapsEvent.MAP_READY).then(() => {
      console.log("Map is ready!");

      // fazer o loop -- achar uma forma do mesmo sempre atualizar-- mapiado a primeira vez, depois ver como
      //ele muda o status e atualiza no papa direto.

      this.vagas.forEach(element => {});



      //fazer o loop

      // Now you can use all methods safely. fazer um for para percorrer as vagas e marcalas
      this.map.addMarker({
        title: "Ionic",
        icon: "blue",
        animation: "DROP",
        position: {
          lat: -19.92494,
          lng: -44.08436
        }
      });
      this.map
        .addMarker({
          title: "Ionic",
          icon: "blue",
          animation: "DROP",
          position: {
            lat: -19.9249,
            lng: -44.08433
          }
        })
        .then(marker => {
          marker.on(GoogleMapsEvent.MARKER_CLICK).subscribe(() => {
            alert("clicked");
          });
        });
    });
  }
}
