import { FirebaseListObservable } from 'angularfire2';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Geolocation } from '@ionic-native/geolocation';

import { VagaService } from './../../providers/vaga/vaga.service';
import { Vaga } from './../../models/vaga.model';


/**
 * Generated class for the MapaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-mapa',
  templateUrl: 'mapa.html',
})
export class MapaPage {

  teste = {
    latitude: null,
    longitude: null
  }

  vagas: FirebaseListObservable<Vaga[]>;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private geolocation: Geolocation,
    public VagaService: VagaService) {

    this.geolocation.getCurrentPosition().then((resp) => {
        this.teste.latitude = resp.coords.latitude
        this.teste.longitude = resp.coords.longitude
        console.log(resp.coords.latitude)
        console.log(resp.coords.longitude)

    }).catch((error) => {
      console.log('Error getting location', error);
    });

    let watch = this.geolocation.watchPosition();
    watch.subscribe((data) => {
      // data can be a set of coordinates, or an error (if an error occurred).
      this.teste.latitude = data.coords.latitude
      this.teste.longitude = data.coords.longitude
      console.log(data.coords.latitude)
      console.log(data.coords.longitude)
    });
  }

    //exibir usuario LISTANDO pode deletar.
  ionViewDidLoad() {
    this.vagas = this.VagaService.vagas;
  }
  //exibir usuario LISTANDO pode deletar.
  onListVaga(vaga: Vaga):void{
    console.log('User: ', vaga);
  }

  //fazer um for

}
