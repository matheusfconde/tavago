import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { SignupPage } from '../signup/signup';

import { User } from './../../models/user.model';
import { FirebaseListObservable } from 'angularfire2';
import { UserService } from './../../providers/user/user.service';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  users: FirebaseListObservable<User[]>;

  constructor(
    public navCtrl: NavController,
    public UserService: UserService,
  ) {

  }
  //exibir usuario LISTANDO pode deletar.
  ionViewDidLoad() {
    this.users = this.UserService.users;
  }
  //exibir usuario LISTANDO pode deletar.
  onListUser(user: User):void{
    console.log('User: ', user);
  }

  onSignup(): void{
    this.navCtrl.push(SignupPage)
  }

}
