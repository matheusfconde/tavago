import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

import { AboutPage } from '../pages/about/about';
import { ContactPage } from '../pages/contact/contact';
import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { MapaPageModule } from '../pages/mapa/mapa.module';
import { Geolocation } from '@ionic-native/geolocation';
import { GooglePageModule } from '../pages/google/google.module';
import { GoogleMaps } from '@ionic-native/google-maps';

import { AngularFireModule, FirebaseAppConfig } from 'angularfire2';
import { LoginPage } from '../pages/login/login';
import { SignupPage } from '../pages/signup/signup';
import { UserService } from '../providers/user/user.service';
import { VagaService } from "../providers/vaga/vaga.service";

const firebaseAppConfig: FirebaseAppConfig = {
    apiKey: "AIzaSyAVNP_our7x4MsrB3AIDh4tvx8W8fsUJhY",
    authDomain: "fir-tavago.firebaseapp.com",
    databaseURL: "https://fir-tavago.firebaseio.com",
    storageBucket: "fir-tavago.appspot.com",
    messagingSenderId: "232314070787"
}

@NgModule({
  declarations: [
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    LoginPage,
    SignupPage,
    TabsPage
  ],
  imports: [
    BrowserModule,
    HttpModule,
    IonicModule.forRoot(MyApp),
    MapaPageModule,
    GooglePageModule,
    AngularFireModule.initializeApp(firebaseAppConfig)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    LoginPage,
    SignupPage,
    TabsPage
  ],
  providers: [
    GoogleMaps,
    Geolocation,
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    UserService,
    VagaService
  ]
})
export class AppModule {}
