import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { AngularFire, FirebaseListObservable } from "angularfire2";
import { Vaga } from './../../models/vaga.model';


/*
  Generated class for the UserProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class VagaService {

  vagas: FirebaseListObservable<Vaga[]>;

  constructor(
    public af: AngularFire,
    public http: Http
  ) {
    console.log('Hello UserProvider Provider');
    //exibir usuario LISTANDO pode deletar.
    this.vagas = this.af.database.list('/vagas');
  }
  create(vaga: Vaga): firebase.Promise<void>{
    return this.af.database.list('/vagas')
    .push(vaga);
  }

}
