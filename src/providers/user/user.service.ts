import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { AngularFire, FirebaseListObservable } from "angularfire2";
import { User } from '../../models/user.model';


/*
  Generated class for the UserProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class UserService {

  users: FirebaseListObservable<User[]>;

  constructor(
    public af: AngularFire,
    public http: Http
  ) {
    console.log('Hello UserProvider Provider');
    //exibir usuario LISTANDO pode deletar.
    this.users = this.af.database.list('/users');
  }
  create(user: User): firebase.Promise<void>{
    return this.af.database.list('/users')
    .push(user);
  }

}
